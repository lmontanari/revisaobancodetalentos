public class Gerente extends Funcionario implements Tributavel, Autenticavel {

    private int senha;

    private int numeroDeFuncionariosGerenciados;

    public Gerente(String nome, String cpf, double salario, int senha, int numeroDeFuncionariosGerenciados) {
        super(nome, cpf, salario);
        this.senha = senha;
        this.numeroDeFuncionariosGerenciados = numeroDeFuncionariosGerenciados;
    }

    public double getPLR() {
        return this.getSalario() * 0.15;
    }

    @Override
    public double calculaTributo() {
        return (this.getSalario() * 0.27) + (this.getPLR() * 0.05);
    }

    @Override
    public boolean autentica(int senha) {
        // faz autenticacao padrão
        return this.senha == senha;
    }
}
