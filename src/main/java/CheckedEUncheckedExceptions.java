public class CheckedEUncheckedExceptions {

    public static void main(String args[]) {

        try {
            sacaDinheiro(100);
        } catch (SaldoInsuficienteException e) {
            // Se não tiver dinheiro vou dar um emprestimo de mil reais.
            // depositaDinheiro(1_000);
            // depois: sacaDinheiro(100);
        }

        // Veja não é necessario try/catch
        depositaDinheiro(100);
    }

    private static void sacaDinheiro(int valor) throws SaldoInsuficienteException {
        int saldo = 100;
        if(valor > saldo) {

            // Veja essa é uma situação que pode acontecer, temos que nos previnir.
            // Note também que esse método não sabe lidar com essa situação.. ele pode ter valor negativo? Não sabemos.
            // Será que nesse caso o sistema vai dar um empréstimo? é muita informação pra esse método...
            // Logo "subimos" essa responsabilidade pra quem nos chamou, esse metodo ou resolve ou delega tbm.
            throw new SaldoInsuficienteException();
        }


        // faz o saque.
    }


    private static void depositaDinheiro(int valor) {

        // Situação maluca
        // Não permitimos esse tipo de coisa. não faz parte do nosso mundo de possibilidades.
        // Vamos validar e estourar tudo porque isso é inválido.
        if(valor < 0) {
            throw new IllegalArgumentException("Poxa não pode nunca depositar valor negativo, isso é gambis");
        }

        // faz o depósito.
    }
}
