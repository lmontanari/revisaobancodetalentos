public class Estagiario extends Funcionario {

    private String faculdade;

    public Estagiario(String nome, String cpf, double salario, String faculdade) {
        super(nome, cpf, salario);
        this.faculdade = faculdade;
    }

}
