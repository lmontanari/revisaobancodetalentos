import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Streams {

    public static void main(String args[]) {

        Funcionario f1 = new Funcionario("Joao", "33344455566", 5_000);
        Funcionario f2 = new Gerente("Maria", "11144455566", 10_000, 123456, 10);
        Funcionario f3 = new Estagiario("Jose", "22244455566", 1_000, "Unicamp");


        List<Funcionario> listagemFuncionariosDaEmpresa = new ArrayList<>();

        listagemFuncionariosDaEmpresa.add(f1);
        listagemFuncionariosDaEmpresa.add(f2);
        listagemFuncionariosDaEmpresa.add(f3);

        // Default method:
        // Comparator Nâo é interface funcional.
        listagemFuncionariosDaEmpresa.sort((o1,o2) -> new Double(o1.getSalario()).compareTo(o2.getSalario()));

        for (Funcionario func: listagemFuncionariosDaEmpresa) {
            System.out.println(func.getNome() + " salario=" + func.getSalario());
        }
        System.out.println("----------------------------------------");

        // Streams
        listagemFuncionariosDaEmpresa = new ArrayList<>();

        listagemFuncionariosDaEmpresa.add(f1);
        listagemFuncionariosDaEmpresa.add(f2);
        listagemFuncionariosDaEmpresa.add(f3);

        // Funcional interface em filter.
        List<Funcionario> novaListagem = listagemFuncionariosDaEmpresa.stream().filter(f -> !(f instanceof Estagiario)).sorted((o1, o2) -> new Double(o1.getSalario()).compareTo(o2.getSalario())).collect(Collectors.toList());

        for (Funcionario func: listagemFuncionariosDaEmpresa) {
            System.out.println(func.getNome() + " salario=" + func.getSalario());
        }
        System.out.println("----------------------------------------");

        novaListagem.stream().forEach(f -> System.out.println(f.getNome() + " salario=" + f.getSalario()));
        System.out.println("----------------------------------------");

        Gerente g1 = new Gerente("Chapolin", "11144455566", 10_000, 123456, 10);
        Gerente g2 = new Gerente("Racha cuca", "11144455566", 10_000, 111111, 10);
        Gerente g3 = new Gerente("Pepe", "11144455566", 10_000, 222222, 10);

        List<Gerente> listagemDeGerentes = new ArrayList<>();

        listagemDeGerentes.add(g1);
        listagemDeGerentes.add(g2);
        listagemDeGerentes.add(g3);

        // Funcional interface em filter.
        // Somente gerentes que autentiquem corretamente.
        int SENHA_AUTENTICACAO = 123456;
        listagemDeGerentes.stream().filter(g -> g.autentica(SENHA_AUTENTICACAO)).forEach(f -> System.out.println(f.getNome() + " salario=" + f.getSalario()));

    }



}
