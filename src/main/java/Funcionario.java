public class Funcionario implements Tributavel {

    private String nome;
    private String cpf;
    private double salario;

    public Funcionario(String nome, String cpf, double salario) {
        this.nome = nome;
        this.cpf = cpf;
        this.salario = salario;
    }

    public String getNome() {
        return nome;
    }

    public double getSalario() {
        return salario;
    }

    public double getPLR() {
        return this.salario * 0.10;
    }

    @Override
    public double calculaTributo() {
        return this.salario * 0.27;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Funcionario that = (Funcionario) o;

        if (Double.compare(that.salario, salario) != 0) return false;
        if (!nome.equals(that.nome)) return false;
        return cpf.equals(that.cpf);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = nome.hashCode();
        result = 31 * result + cpf.hashCode();
        temp = Double.doubleToLongBits(salario);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
