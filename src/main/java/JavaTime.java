import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class JavaTime {

    public static void main (String args[]) {

        // jeito antigo carnaval.
        System.out.println("----- JEITO ANTIGO --------------");
        Calendar instance = GregorianCalendar.getInstance();
        instance.set(Calendar.MONTH, Calendar.MARCH);
        instance.set(Calendar.DAY_OF_MONTH, 05);
        instance.set(Calendar.YEAR, 2019);
        Date carnaval = instance.getTime();

        System.out.println(new SimpleDateFormat("dd-MM-YYYY").format(carnaval));

        Calendar instance2 = GregorianCalendar.getInstance();
        instance2.setTime(carnaval);
        instance.add(Calendar.DAY_OF_MONTH, 1);
        Date quartaFeiraDeCinzas = instance.getTime();
        System.out.println(new SimpleDateFormat("dd-MM-YYYY").format(quartaFeiraDeCinzas));

        // Jeito java 8
        System.out.println("----- JEITO JAVA 8 --------------");
        LocalDate carnaval18 = LocalDate.of(2019, Month.MARCH, 5);
        System.out.println(carnaval18.format(DateTimeFormatter.ISO_DATE));

        LocalDate quartaFeiraDeCinzas18 = carnaval18.plusDays(1);
        System.out.println(quartaFeiraDeCinzas18.format(DateTimeFormatter.ISO_DATE));
    }
}
