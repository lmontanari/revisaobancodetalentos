public class ExplicacaoInterface {

    public static void main(String args[]) {
        //Autenticavel f1 = new Funcionario("Joao", "33344455566", 5_000);
        Autenticavel f2 = new Gerente("Maria", "11144455566", 10_000, 123456, 10);
        // Autenticavel f3 = new Estagiario("Jose", "22244455566", 1_000, "Unicamp");

        abreEssaPorta(f2);


        Funcionario f22 = new Gerente("Kiko", "11144455566", 50_000, 123456, 10);
        // abreEssaPorta(f22); // Não compila

        Gerente f222 = new Gerente("Chiquinha", "11144455566", 500, 111855, 10);
        abreEssaPorta(f222); // Aí compila


        // Interfaces contém métodos (comportamento), não atributos.
        // Pense eum uma interface como sendo um adjetivo ou um advérbio
        // (algo que modifica um substantivo ou adjetivo) e nas classes como sendo substantivos.


        // adjetivo
        // palavra que se junta ao substantivo para modificar o seu significado,
        // acrescentando-lhe noções de qualidade, natureza, estado etc.

        // adverbio
        // palavra invariável que funciona como um modificador de um verbo ( dormir pouco),
        // um adjetivo ( muito bom ), um outro advérbio ( deveras astuciosamente ),
        // uma frase ( felizmente ele chegou ), exprimindo circunstância de tempo,
        // modo, lugar, qualidade, causa, intensidade, oposição, afirmação, negação, dúvida, aprovação etc.

    }

    private static void abreEssaPorta(Autenticavel autenticavel) {

        int SENHA_DA_PORTA = 123456;

        if (autenticavel.autentica(SENHA_DA_PORTA)) {
            System.out.println("A porta abriu");
        } else {
            System.out.println("A porta NÃO abriu");
        }

    }
}
