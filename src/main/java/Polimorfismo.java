import java.util.Arrays;
import java.util.List;

public class Polimorfismo {

    public static void main(String args[]) {
        //Polimorfismo é a capacidade de um objeto poder ser referenciado de várias formas.
        // (cuidado, polimorfismo não quer dizer que o objeto fica se transformando, muito pelo contrário,
        // um objeto nasce de um tipo e morre daquele tipo, o que pode mudar é a maneira como nos referimos a ele).


        //não importa como nos referenciamos a um objeto, o método que será invocado é sempre o que é dele

        // É exatamente esse o poder do polimorfismo, juntamente com a reescrita de método:
        // diminuir o acoplamento entre as classes, para evitar que novos códigos resultem em modificações em inúmeros lugares.

        Funcionario f1 = new Funcionario("Joao", "33344455566", 5_000);
        Funcionario f2 = new Gerente("Maria", "11144455566", 10_000, 123456, 10);
        Funcionario f3 = new Estagiario("Jose", "22244455566", 1_000, "Unicamp");

        System.out.println("Funcionario 1 recebe = " + f1.getPLR() + " de plr. Já o funcionário 2 recebe = " + f2.getPLR());

        List<Tributavel> todosTributaveisDaEmpresa = Arrays.asList(f1, f2, f3);
        todosTributaveisDaEmpresa.stream().forEach(t -> System.out.println(t.calculaTributo()));

        // Será que queremos que o estagiário ser tributado?

        // Note que o uso de herança aumenta o acoplamento entre as classes, isto é,
        // o quanto uma classe depende de outra. A relação entre classe mãe e filha é muito forte e
        // isso acaba fazendo com que o programador das classes filhas tenha que conhecer a implementação da
        // classe mãe e vice-versa - fica difícil fazer uma mudança pontual no sistema.

        // herança versus composição
    }
}
