import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SetListEMap {

    public static void main(String args[]) {

        // Set
        // My mother bought a new knife set.
        // Conjunto
        // Conhecemos conjuntos como uma coleção ou grupos de objetos ou símbolos aos quais chamamos de elementos.
        //     O conjunto de todos os alunos de uma sala (A);
        //    O conjunto musical (M);
        //    O conjunto dos números inteiros (Ζ);
        //    O conjunto dos números naturais (Ν).

        Funcionario f1 = new Funcionario("Joao", "33344455566", 5_000);
        Funcionario f2 = new Gerente("Maria", "11144455566", 10_000, 123456, 10);
        Funcionario f3 = new Estagiario("Jose", "22244455566", 1_000, "Unicamp");

        // It makes no guarantees as to the iteration order of the set; in particular, it does not guarantee that the
        // order will remain constant over time.
        Set<Funcionario> todosFuncionariosDaEmpresa = new HashSet<>();

        todosFuncionariosDaEmpresa.add(f1);
        todosFuncionariosDaEmpresa.add(f2);
        todosFuncionariosDaEmpresa.add(f3);

        todosFuncionariosDaEmpresa.stream().forEach(t -> {System.out.print(t.getNome()); System.out.print(",");});
        System.out.println("");
        System.out.println("-------------------------------------");
        List<Funcionario> listagemFuncionariosDaEmpresa = new ArrayList<>();

        listagemFuncionariosDaEmpresa.add(f1);
        listagemFuncionariosDaEmpresa.add(f2);
        listagemFuncionariosDaEmpresa.add(f3);


        listagemFuncionariosDaEmpresa.stream().forEach(t -> {System.out.print(t.getNome()); System.out.print(",");});
        System.out.println("");
        System.out.println("-------------------------------------");

        Map<String, Funcionario> mapFuncionariosDaEmpresa = new HashMap();

        mapFuncionariosDaEmpresa.put(f1.getNome(), f1);
        mapFuncionariosDaEmpresa.put(f2.getNome(), f2);
        mapFuncionariosDaEmpresa.put(f3.getNome(), f3);

        System.out.println(mapFuncionariosDaEmpresa.values());
        System.out.println("-------------------------------------");
    }
}
