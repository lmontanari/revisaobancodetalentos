import java.util.HashSet;
import java.util.Set;

public class ImportanceOfEqualsAndHashCode {

    public static void main(String[] args) {

        Pessoa p1 = new Pessoa(31, "João");
        Pessoa p2 = new Pessoa(18, "Maria");
        Pessoa p3 = new Pessoa(18, "Maria");


//        System.out.println("Comparando usando == : " + (p2 == p2));

//        System.out.println("Comparando " + p1.getNome() + " e " + p2.getNome() + " usando equals: " + (p1.equals(p2)));
//        System.out.println("Comparando " + p1.getNome() + " e " + p2.getNome() + " usando == : " + (p1 == p2));
//
//        System.out.println("Comparando " + p3.getNome() + " e " + p2.getNome() + " usando == : " + (p3 == p2));
//        System.out.println("Comparando " + p3.getNome() + " e " + p2.getNome() + " usando == : " + (p3.equals(p2)));

        System.out.println("Hashcode de " + p1.getNome() + "  == : " + p1.hashCode());
        System.out.println("Hashcode de " + p2.getNome() + "  == : " + p2.hashCode());
        System.out.println("Hashcode de " + p3.getNome() + " == : " + p3.hashCode());

        Set set = new HashSet<>();

        set.add(p1);
        System.out.println("Size do set após adicionar p1=" + set.size());

        set.add(p2);
        System.out.println("Size do set após adicionar p2=" + set.size());

        set.add(p3);
        System.out.println("Size do set após adicionar p3=" + set.size());


        PessoaComHashCodeRandomico pR1 = new PessoaComHashCodeRandomico(31, "João");
        PessoaComHashCodeRandomico pR2 = new PessoaComHashCodeRandomico(18, "Maria");
        PessoaComHashCodeRandomico pR3 = new PessoaComHashCodeRandomico(18, "Maria");
        System.out.println("Hashcode de " + pR1.getNome() + "  == : " + pR1.hashCode());
        System.out.println("Hashcode de " + pR2.getNome() + "  == : " + pR2.hashCode());
        System.out.println("Hashcode de " + pR3.getNome() + " == : " + pR3.hashCode());

        set = new HashSet<>();

        set.add(pR1);
        System.out.println("Size do set após adicionar pR1=" + set.size());

        set.add(pR2);
        System.out.println("Size do set após adicionar pR2=" + set.size());

        set.add(pR3);
        System.out.println("Size do set após adicionar pR3=" + set.size());
    }
}
