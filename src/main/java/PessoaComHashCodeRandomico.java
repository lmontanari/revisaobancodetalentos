import java.util.UUID;

public class PessoaComHashCodeRandomico {

    private int idade;

    private String nome;

    public PessoaComHashCodeRandomico(int idade, String nome) {
        this.idade = idade;
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    // ---- Somente hashcode errado ---//
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PessoaComHashCodeRandomico pessoa = (PessoaComHashCodeRandomico) o;

        if (idade != pessoa.idade) return false;
        return nome != null ? nome.equals(pessoa.nome) : pessoa.nome == null;
    }

    @Override
    public int hashCode() {
        return (int) UUID.randomUUID().getMostSignificantBits();
    }
    // ---- Somente hashcode errado ---//



}
